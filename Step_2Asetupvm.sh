#Update the system:
sudo yum update

#Install Node.js and npm:
curl -sL https://rpm.nodesource.com/setup_14.x | sudo bash -
sudo yum install -y nodejs

#Verify the installation:
node -v
npm -v

#Install the C++ compiler (GCC): 
sudo yum install gcc

#nstall the C++ standard library: 
sudo yum install gcc-c++

#Set up a build system (optional):
sudo yum install make

git config --global user.email "lamnguyen20102005@gmail.com"
git config --global user.name "lamnguyen20102005"